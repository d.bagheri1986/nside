package models;

public class Product {
    private String label;
    private String shipmentType;

    public Product(String label, String shipmentType) {
        this.label = label;
        this.shipmentType = shipmentType;
    }

    public Product(){};

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }
}
