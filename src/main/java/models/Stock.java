package models;

public class Stock {

    private String depot;
    private String product;
    private String quantity;
    private String expiry;

    public Stock(String depot, String product, String quantity, String expiry) {
        this.depot = depot;
        this.product = product;
        this.quantity = quantity;
        this.expiry = expiry;
    }

    public Stock(){};

    public String getDepot() {
        return depot;
    }

    public void setDepot(String depot) {
        this.depot = depot;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }
}
