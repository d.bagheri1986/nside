package models;

import java.util.List;

public class DataBaseModel {
    private List<Product> products;
    private List<Network> networks;
    private List<Stock> stocks;
    private List<OngoingShipment> ongoingShipments;


    public DataBaseModel(List<Product> products, List<Network> networks, List<Stock> stocks,
        List<OngoingShipment> ongoingShipments) {
        this.products = products;
        this.networks = networks;
        this.stocks = stocks;
        this.ongoingShipments = ongoingShipments;
    }

    public DataBaseModel(){};


    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Network> getNetworks() {
        return networks;
    }

    public void setNetworks(List<Network> networks) {
        this.networks = networks;
    }

    public List<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(List<Stock> stocks) {
        this.stocks = stocks;
    }

    public List<OngoingShipment> getOngoingShipments() {
        return ongoingShipments;
    }

    public void setOngoingShipments(List<OngoingShipment> ongoingShipments) {
        this.ongoingShipments = ongoingShipments;
    }
}
