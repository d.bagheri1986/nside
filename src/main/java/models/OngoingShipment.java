package models;

public class OngoingShipment {
    private String depotFrom;
    private String depotTo;
    private String departureDate;
    private String product;
    private String quantity;
    private String expiry;

    public OngoingShipment(String depotFrom, String depotTo, String departureDate, String product,
        String quantity, String expiry) {
        this.depotFrom = depotFrom;
        this.depotTo = depotTo;
        this.departureDate = departureDate;
        this.product = product;
        this.quantity = quantity;
        this.expiry = expiry;
    }

    public OngoingShipment(){};

    public String getDepotFrom() {
        return depotFrom;
    }

    public void setDepotFrom(String depotFrom) {
        this.depotFrom = depotFrom;
    }

    public String getDepotTo() {
        return depotTo;
    }

    public void setDepotTo(String depotTo) {
        this.depotTo = depotTo;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }
}
