package models;

public class Network {
    private String depotFrom;
    private String depotTo;

    public Network(String depotFrom, String depotTo) {
        this.depotFrom = depotFrom;
        this.depotTo = depotTo;
    }

    public Network(){};

    public String getDepotFrom() {
        return depotFrom;
    }

    public void setDepotFrom(String depotFrom) {
        this.depotFrom = depotFrom;
    }

    public String getDepotTo() {
        return depotTo;
    }

    public void setDepotTo(String depotTo) {
        this.depotTo = depotTo;
    }
}
