package com.nside.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Timer;

@SpringBootApplication
@RestController
@Configuration
@EnableScheduling
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@Bean
	public ProductExpiryMonitorService bean() throws ParseException {
		return new ProductExpiryMonitorService();
	}

	public static void main(String[] args) throws ParseException {
		SpringApplication.run(DemoApplication.class, args);
	}
}
