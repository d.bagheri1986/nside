package com.nside.demo;

import models.Stock;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class CustomerNotifierSerivce extends SetCalender{

      private int notificationDeadLine = 7;

    public CustomerNotifierSerivce() throws ParseException {
    }

    void notifyCustomer(Stock stock) throws ParseException {
        Date productExpiryDate = sdf.parse(stock.getExpiry());
        if (productExpiryDate.compareTo(calendar.getTime()) <= notificationDeadLine) {
            //Inform customer by Email. Here just a terminal log
            System.out.println(stock.getProduct() + " soon to be expired, in less than 7 days");
        }
    }
}
