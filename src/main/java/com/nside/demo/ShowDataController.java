package com.nside.demo;


import models.DataBaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.sql.DatabaseMetaData;

@Controller
public class ShowDataController {

    @Autowired
    DataBaseReaderService dataBaseReaderService;

    @GetMapping("/data")
    public String showData(Model model){
        DataBaseModel dataBaseModel = getData();
        model.addAttribute("data", dataBaseModel);
        return "dataPage.html";
    }

    private DataBaseModel getData(){
        return dataBaseReaderService.getDataBaseModel();
    }
}
