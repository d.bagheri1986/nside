package com.nside.demo;

import models.DataBaseModel;
import models.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ProductExpiryMonitorService {

    @Autowired
    private DataBaseReaderService dataBaseReaderService;

    @Autowired
    private StockManagerNotifierService stockManagerNotifierService;

    @Autowired
    private CustomerNotifierSerivce customerNotifierSerivce;

    private SimpleDateFormat sdf;
    private Date currentDate;
    private Calendar cal = Calendar.getInstance();
    public ProductExpiryMonitorService() throws ParseException {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        currentDate = sdf.parse("2019-11-30");
        cal.setTime(currentDate);
    }

    @Scheduled(fixedRate=1000*60*60*24)
    @Scheduled(fixedRate=1000*5)
    public void demoServiceMethod() throws ParseException {
        updateCalendars();
        checkProductsExpiryDate();
    }


    private void checkProductsExpiryDate() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        DataBaseModel dataBaseModel = dataBaseReaderService.getDataBaseModel();
        List<Stock> stocks= dataBaseModel.getStocks();
        for (Stock stock:stocks) {
            stockManagerNotifierService.notifyManager(stock);
            customerNotifierSerivce.notifyCustomer(stock);
        }
    }

    public void updateCalendars(){
        cal.add(Calendar.DATE, 1);
        stockManagerNotifierService.updateCalendar();
        customerNotifierSerivce.updateCalendar();
    }

}
