package com.nside.demo;

import models.Stock;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class StockManagerNotifierService extends SetCalender{
    private int notificationDeadLine = 0;

    public StockManagerNotifierService() throws ParseException {
    }

    void notifyManager(Stock stock) throws ParseException {
        Date productExpiryDate = sdf.parse(stock.getExpiry());
        if (productExpiryDate.compareTo(calendar.getTime()) == notificationDeadLine) {
            //Inform Stock Manager by Email. Here just a terminal log
            System.out.println(stock.getProduct() + "expired");
        }
    }

}
