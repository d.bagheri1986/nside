package com.nside.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SetCalender {

    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd");
    private Date currentDate = sdf.parse("2019-11-30");

    public SetCalender() throws ParseException {
        calendar.setTime(currentDate);
    }

    void updateCalendar(){
        calendar.add(Calendar.DATE, 1);
    }
}
