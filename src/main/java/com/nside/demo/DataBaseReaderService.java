package com.nside.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.DataBaseModel;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class DataBaseReaderService {

    private DataBaseModel dataBaseModel;

    public DataBaseReaderService() throws IOException {
        this.dataBaseModel = this.readFileToJson();
    }


    public DataBaseModel getDataBaseModel(){
        return this.dataBaseModel;
    }

    private DataBaseModel readFileToJson() throws IOException {
        Path resourceDirectory = Paths.get("data.json");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        ObjectMapper mapper = new ObjectMapper();
        DataBaseModel dataBaseModel = mapper.readValue(new File(absolutePath), DataBaseModel.class);
        return dataBaseModel;
    }
}
